// import './App.css';
import { useEffect, useState } from 'react';
import 'react-h5-audio-player/lib/styles.css';
import UserList from '../components/UserList';
import UserDetail from '../components/UserDetail';
import { randomIntFromInterval } from '../handlers/utils'
import APIendPoints from '../constants/APIendPoints';

function Home() {

  const [users, setUsers] = useState([])
  const [loaded, setLoaded] = useState(false)
  const [selectedUser, setSelectedUser] = useState(null)
  useEffect(() => {
    fetch(APIendPoints.baseURL + APIendPoints.users)
      .then(response => response.json())
      .then(data => {
        if (data.success && data.data) {
          let users = data.data.map((u) => {
            return {
              ...u,
              avatarId: randomIntFromInterval(1, 7)
            }
          })
          setUsers([...users])
        }
        setTimeout(() => {
          setLoaded(true)
        }, 1000);
      }).catch(err => {
        setLoaded(true)
      })

    return () => { }
  }, [])



  return (
    <>
      <div className="container">
        <div className="row justify-content-center">
          {!selectedUser ? <UserList loaded={loaded} users={users} setSelectedUser={setSelectedUser} /> : <UserDetail selectedUser={selectedUser} setSelectedUser={setSelectedUser} />}
        </div>
      </div>
    </>
  );
}

export default Home;
