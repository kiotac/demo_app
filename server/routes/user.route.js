// importing modules
const express = require('express')
const router = express.Router();
const { updateUser,
    createUser,
    getUsers,
    getUserById,
    loadAudio
} = require('../controllers/user.controller')

const multer = require('multer')
const upload = multer({ dest: './public/uploads/' })

const { celebrate, Joi, Segments } = require('celebrate');


router.post("/", celebrate({
    [Segments.BODY]: Joi.object().keys({
        name: Joi.string().required(),
        email: Joi.string().email().required(),
        phoneNumber: Joi.string().required(),
        // isAdmin: Joi.boolean().default(false)
    })
}), createUser);

router.get("/", getUsers);

router.get("/:userId", celebrate({
    [Segments.PARAMS]: Joi.object().keys({
        userId: Joi.string().required(),
    })
}), getUserById);

router.put("/", upload.single('uploaded_file'), celebrate({
    [Segments.BODY]: Joi.object().keys({
        userId: Joi.string().required()
    })
}), updateUser);

router.get('/download/:audioId', celebrate({
    [Segments.PARAMS]: Joi.object().keys({
        audioId: Joi.string().required()
    })
}), loadAudio);

module.exports = router;
