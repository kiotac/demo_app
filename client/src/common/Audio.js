import AudioPlayer from 'react-h5-audio-player';
import 'react-h5-audio-player/lib/styles.css';
import { baseURL, download } from '../constants/APIendPoints';

export default function Audio({ audio, audioType }) {
    return (
        <AudioPlayer
            key={audio.filename}
            autoPlay={false}
            src={`${baseURL}${download}${audio.filename}.${audioType}`}
            // onPlay={e => console.log("onPlay")}
            style={{ borderWidth: 0 }}
        // onPlay={() => setPlay(i)}
        // other props here
        />
    )
}