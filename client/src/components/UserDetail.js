import moment from 'moment';
import Audio from '../common/Audio'

export default function UserDetail({selectedUser, setSelectedUser}) {
    return (
        <div className="row vh-100 align-items-center justify-content-center">
              <h4 className='text-center'>USER DETAIL</h4>
              <div class="card w-100">
                <div className='d-flex justify-content-end'>
                  <i class="fas fa-times-circle m-3 fs-3" onClick={() => setSelectedUser(null)}></i>
                </div>
                <img src={require(`../assets/${selectedUser.avatarId}.jpeg`)} class="card-img-top  rounded mx-auto d-block w-25" alt="..." />
                <div className="card-body">
                  <h5 className="card-title text-center">{selectedUser.name}</h5>
                  <p className="card-text  text-center">{selectedUser.phoneNumber}</p>
                  <p className="card-text  text-center">{selectedUser.email}</p>
                  <h5>Audio Files</h5>
                  {selectedUser && selectedUser.audios && selectedUser.audios.map((audio, i) => {
                    let audioType= audio.originalname.split('.')[audio.originalname.split('.').length - 1]
                    if (audioType !== 'mp3') {
                      return;
                    }
                    return (
                      <div className='row w-100 mt-3'>
                        <Audio audio={audio} audioType={audioType}/>
                      </div>
                    )
                  })}
                </div>
                <div className="card-footer">
                  <small className="text-muted">User created {moment(selectedUser.createdAt).fromNow()}</small>
                </div>
              </div>
            </div>
    )
}