const Users = require('../models/user.model')
const { dirname } = require('path');
const appDir = dirname(require.main.filename);

const createUser = async (req, res) => {
    try {
        const user = await Users.create({ ...req.body });
        return res.status(200).json({ success: true, data: user });
    } catch (err) {
        return res.status(400).json({ success: false, message: err.message, data: null });
    }
};

const getUsers = async (req, res) => {
    try {
        const user = await Users.find({});
        return res.status(200).json({ success: true, data: user });
    } catch (err) {
        return res.status(400).json({ success: false, message: err.message, data: null });
    }
};

const getUserById = async (req, res) => {
    try {
        const user = await Users.findOne({ _id: req.params.userId });
        return res.status(200).json({ success: true, data: user });
    } catch (err) {
        return res.status(400).json({ success: false, message: err.message, data: null });
    }
};




const updateUser = async (req, res) => {
    console.log("S-s-s>>>>>>>>>>>>>>>>userrr", req.file.filename)

    try {
        const user = await Users.updateOne({
            _id: req.body.userId
        },
            { $push: { audios: req.file } }
        );
        return res.status(200).json({ success: true, data: user });
    } catch (err) {
        return res.status(400).json({ success: false, message: err.message, data: null });
    }
};

const loadAudio = async (req, res) => {
    const audioId = req.params.audioId.split('.')[0]
    const file = `${appDir}/public/uploads/${audioId}`;
    res.download(file);
};

module.exports = { createUser, getUsers, updateUser, getUserById, loadAudio };