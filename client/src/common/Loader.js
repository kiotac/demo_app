import moment from 'moment';

export default function Loader() {
    return (
<div class="d-flex vh-100 justify-content-center align-items-center">
            <div class=" spinner-grow text-info" role="status">
              <span class="sr-only"></span>
            </div>
          </div>
    )
}