import moment from 'moment';

export default function Card({user, setSelectedUser}) {
    return (
        <div class="card-group text-center pt-3" style={{ width: '22rem' }} onClick={() => setSelectedUser(user)}>
            <div class="card ">
                <img src={require(`../assets/${user.avatarId}.jpeg`)} class="card-img-top w-50 rounded mx-auto d-block" alt="logo" />
                <div className="card-body">
                    <h5 className="card-title">{user.name}</h5>
                    <p className="card-text">{user.phoneNumber}</p>
                </div>
                <div className="card-footer">
                    <small className="text-muted">Created {moment(user.createdAt).fromNow()}</small>
                </div>
            </div>
        </div>
    )
}