require('dotenv').config();
const express = require('express');
const http = require("http");
var cors = require('cors')
const connectDB = require('./db/connect')
const UserRouter = require('./routes/user.route')
const { errors } = require('celebrate');


const app = express();

app.use(cors())

const port = process.env.PORT || 5000

// build-in middleware
app.use(express.json());

app.use('/api/v1/users', UserRouter);

// application middlewares
app.use('*', (req, res) => {
    res.status(404).send({
        message: 'No route found!'
    })
});

app.use(errors());


const start = async () => {
    try {
        await connectDB(process.env.MONGO_URI);
        app.listen(port, () => console.log(`App listening on port ${port}!`));
    } catch (err) {
        console.log(err);
    }
}
start();
