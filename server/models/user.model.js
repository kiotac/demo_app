const mongoose = require('mongoose')

// schema with validation
const Schema = mongoose.Schema;

const UsersSchema = new Schema({
    name: {
        type: String, required: [true, 'name must be provided'],
        max: [50, 'cannot be more than 50 characters'], trim: true,
    },
    email: {
        type: String,
        required: [true, 'Email must be provided'],
        trim: true,
        unique: true,
        lowercase: true,
    },
    phoneNumber: {
        type: String,
        required: [true, 'Phone number must be provided'],
        max: [10, 'cannot be more than 10 characters'],
        trim: true,
        unique: true
    },
    audios: {
        type: Array,
        required: false,
        default: []
    }
},
    { timestamps: true });

module.exports = mongoose.model('Users', UsersSchema);