import moment from 'moment';
import Loader from '../common/Loader';
import Card from '../common/Card'
import NoUserFound from '../common/NoUserFound';

export default function UserList({ users, loaded, setSelectedUser }) {
    return (
        <>
            <h4 className='text-center mt-3'>USER LIST</h4>
            <div className="row w-100 justify-content-center">
                {loaded ? users.length > 0 ? users.map((user) => {
                    return (
                        <Card user={user} setSelectedUser={setSelectedUser} />
                    )
                }) : <NoUserFound /> : <Loader />}
            </div>

        </>
    )
}